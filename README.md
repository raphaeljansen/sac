# sac
Projeto para acompanhamento dos chamados de um sac

## Depêndencias
* [docker](https://www.docker.com/)
> Para instalar o docker navegue através do link do seu sistema operacional e siga as instruções
    * [Mac](https://www.docker.com/docker-mac)
    * [Fedora](https://www.docker.com/docker-fedora)
    * [Debian](https://www.docker.com/docker-debian)
    * [Ubuntu](https://www.docker.com/docker-ubuntu)
    * [RHEL](https://www.docker.com/docker-red-hat-enterprise-linux-rhel)
* [make](https://www.gnu.org/software/make/)
    * Mac
        > Instale a command line tools através das intruções do [link](http://railsapps.github.io/xcode-command-line-tools.html)
    * Linux
        > Instale através do seu gestor de pacotes preferido: 
        * Apt: `apt-get install make`
        * Dnf: `dnf install make`
        * Yum: `yum install make`
* [awk](https://www.gnu.org/software/gawk/manual/gawk.html)
> Dentro dos meus testes o awk sempre este presente na instalação padrão do so, avise caso tenha algum problema com i awk

## O Projeto
O sistema foi desenvolvido através de containers, desta forma fica mais fácil a interoperabilidade em os diversos sistema operacionais, a configuração, o deploy e a automação.
Para o back end selecionei [Python 3](https://www.python.org/download/releases/3.0/) com [Async](https://docs.python.org/3/library/asyncio.html), [Tornado](http://www.tornadoweb.org/en/stable/), [SQLAlchemy](https://www.sqlalchemy.org/) e [PostgreSQL](https://www.postgresql.org/) como banco de dados.
Para o front end selecionei [ReactJS](https://reactjs.org/) usando [React Scripts](https://www.npmjs.com/package/react-scripts) para facilitar o build e a automação.
Como web server escolhi o [Nginx](https://www.nginx.com/) fazendo proxy para o be e servindo os estáticos do fe, desta forma os projetos executam embaixo do mesmo domínio não necessitando de CORS.
Tentei fazer o melhor no tempo q tive com a experiência que possuo, entretanto muita coisa ficou de fora e visando esclarecer algumas que identifiquei segue a lista abaixo:

* Migration
> Apesar do anunciado informar que a migration seria um diferencial optei por não implementar vist oque o docker automatizou bem a execução dos scripts de banco.
> É claro que a automatização proporcionada pelo docker não resolve a tarefa de migração de banco, entretando o problema em questão é simples num caso mais complexo a migration faria falta.
> Acredito que no caso de implmentar as migrations o framework [Alembic](http://alembic.zzzcomputing.com/en/latest/) poderia ser usado, até mesmo pq ele se integra bem ao [SQLAlchemy](https://www.sqlalchemy.org/).
* Falta "e2e tests"
> Em função do tempo não foi possível implementar os tests e2e. Com mais tempo certamente seria algo a melhorar no projeto
* Melhorar os "unit tests" do fe
> Em função do tempo não consegui produzir a melhor cobertura de tests no projeto fe, o projeto carece de bem amis unit test
* Melhoria na interface visual e versão responsiva
> Não consegui dar muita atenção na interface o que me deixa bastante frustado, gostaria de mais tempo para trabalhar neste quesito

## Executando
A execução consistem em buildar e executar alguns containers ou um compose de containers, depende qual projetos vc deseja executar.
Para executar faça:
> `make run`

Caso vc queira executar cada projeto separado entre na pasta do respectivo e execute conforme as intruções abaixo:
> Obs: Cadas projeto tem suas depências. A automatização criada fará a inicialização e start de cadas depências necessária para a execução do projeto

* Fe, cwd=fe/sac_fe, depende do be
> `make docker_run_deps`

* Be, cwd=be/sac_be, depende do db
> `make docker_run_deps`

## Testes
A execução dos tests unitários utiliza mock e depende apenas do ambiente de desenvolvimento criado
Para criar o ambiente de tests faça:
>  `make dev`

Para executar os tests unitários faça:
> `make test`

Para executar os tests integrados faça:
> `make itest`

## Tarefas Communs
Todos os projeto contam com as tarefas abaixo, basta entrar na pasta do mesmo e executar o comando conforme a listagem

* Clean `make clean`
* Cria o ambiente de dev `make dev`
* Unit Test `make test`
* Integration Test `make itest`
* Coverage `make coverage`
* Run `make run`
* Stop `make stop`

