# sac_fe
Projeto front end para acompanhamento dos chamados de um sac

## Depêndencias
* [docker](https://www.docker.com/)
> Para instalar o docker navegue através do link do seu sistema operacional e siga as instruções
    * [Mac](https://www.docker.com/docker-mac)
    * [Fedora](https://www.docker.com/docker-fedora)
    * [Debian](https://www.docker.com/docker-debian)
    * [Ubuntu](https://www.docker.com/docker-ubuntu)
    * [RHEL](https://www.docker.com/docker-red-hat-enterprise-linux-rhel)
* [make](https://www.gnu.org/software/make/)
    * Mac
        > Instale a command line tools através das intruções do [link](http://railsapps.github.io/xcode-command-line-tools.html)
    * Linux
        > Instale através do seu gestor de pacotes preferido:
        * Apt: `apt-get install make`
        * Dnf: `dnf install make`
        * Yum: `yum install make`
* [awk](https://www.gnu.org/software/gawk/manual/gawk.html)
> Dentro dos meus testes o awk sempre este presente na instalação padrão do so, avise caso tenha algum problema com i awk
* [Node](https://nodejs.org/en/)
* [ReactJS](https://reactjs.org/)
* [React Scripts](https://www.npmjs.com/package/react-scripts)
* [Nginx](https://www.nginx.com/)

## Tarefas

* Clean `make clean`
* Docker Run `make docker_run`
* Docker kill `make docker_stop`
* Docker Run `make docker_run_deps`
* Docker kill `make docker_stop_deps`
* Cria o build estático `make release`
* Cria o ambiente de dev `make dev`
* Unit Test `make test`
* Unit Test `make wtest`
* Integration Test `make itest`
* Coverage `make coverage`
* Run `make run`
* Stop `make stop`

