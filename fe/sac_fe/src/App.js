import React from "react";
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import Form from './Form.js';
import Report from './Report.js';
import './App.css';

class App extends React.Component {

  render(){
    return(
        <div className="App">
            <BrowserRouter>
                <div style={{margin: '10px'}}>
                    <header style={{margin: '10px 5px 10px 5px'}}>
                        <nav>
                            <Link to="/atendimento">Atendimento</Link>{' '}
                            <Link to="/relatorio">Relatório</Link>
                        </nav>
                    </header>
                    <Switch>
                        <Route path="/atendimento" component={Form} />
                        <Route path="/relatorio" render={(props) => (
                            <Report {...props} />
                        )}/>
                    </Switch>
                </div>
            </BrowserRouter>
        </div>
    );
  }
}

export default App;
