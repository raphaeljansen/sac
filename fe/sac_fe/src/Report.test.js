import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Report from './Report.js';

const mock = (status, response) => {
    return new window.Response(JSON.stringify(response), {
        status: status,
        headers: {
            'Content-type': 'application/json'
        }
    });
};

it('renders without crashing', () => {
    window.fetch = jest.fn().mockImplementation(() => Promise.resolve(mock(200, {result: []})));

    const component = renderer.create(
        <Report />
    );
    let model = component.toJSON();
    expect(model).toMatchSnapshot();
});

it('renders with data without crashing', () => {
    let mockResult = {"result": [{"day_created_at": "24/10/2017", "data": [{"day_created_at": "2017-10-24T00:00:00", "state": "RJ", "data": [{"type": "chat", "state": "RJ", "message": "chat xa xe xi rj"}]}, {"day_created_at": "2017-10-24T00:00:00", "state": "SP", "data": [{"type": "email", "state": "SP", "message": "email ble ble sp..."}]}]}, {"day_created_at": "23/10/2017", "data": [{"day_created_at": "2017-10-23T00:00:00", "state": "RJ", "data": [{"type": "email", "state": "RJ", "message": "hoje email chato pra ler rj"}, {"type": "email", "state": "RJ", "message": "hoje email chato pra ler rj"}]}, {"day_created_at": "2017-10-23T00:00:00", "state": "SC", "data": [{"type": "chat", "state": "SC", "message": "bla bla bla chat pra ti sc"}, {"type": "chat", "state": "SC", "message": "bla bla bla chat pra ti sc"}]}, {"day_created_at": "2017-10-23T00:00:00", "state": "SP", "data": [{"type": "telefone", "state": "SP", "message": "hoje bla bla bla sp"}, {"type": "telefone", "state": "SP", "message": "hoje bla bla bla sp"}]}]}, {"day_created_at": "22/10/2017", "data": [{"day_created_at": "2017-10-22T00:00:00", "state": "RJ", "data": [{"type": "telefone", "state": "RJ", "message": "bla bla bla"}, {"type": "telefone", "state": "RJ", "message": "bla bla bla"}]}, {"day_created_at": "2017-10-22T00:00:00", "state": "SC", "data": [{"type": "telefone", "state": "SC", "message": "bla bla bla"}, {"type": "telefone", "state": "SC", "message": "bla bla bla"}, {"type": "telefone", "state": "SC", "message": "bla bla bla"}]}, {"day_created_at": "2017-10-22T00:00:00", "state": "SP", "data": [{"type": "telefone", "state": "SP", "message": "bla bla bla"}, {"type": "telefone", "state": "SP", "message": "bla bla bla"}, {"type": "telefone", "state": "SP", "message": "bla bla bla"}]}]}]};

    window.fetch = jest.fn().mockImplementation(() => Promise.resolve(mock(200, mockResult)));

    const component = renderer.create(
        <Report />
    );
    let model = component.toJSON();
    expect(model).toMatchSnapshot();
});
