import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import ReactTestUtils from 'react-dom/test-utils';
import Form from './Form.js';

const mock = (status, response) => {
    return new window.Response(JSON.stringify(response), {
        status: status,
        headers: {
            'Content-type': 'application/json'
        }
    });
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

it('renders without crashing', () => {
    const component = renderer.create(
        <Form />
    );
    let model = component.toJSON();
    expect(model).toMatchSnapshot();
});

it('does not submit a invalid form', () => {
    window.fetch = jest.fn();
    const component = ReactTestUtils.renderIntoDocument(
        <Form />
    );
    let btnSave = ReactTestUtils.findRenderedDOMComponentWithClass(component, 'form_btn_submit');
    ReactTestUtils.Simulate.submit(btnSave,
        {
            preventDefault: jest.fn()
        }
    );

    expect(window.fetch).not.toHaveBeenCalled();
});

it('save a new record', () => {
   let mock_result = {
        id: 'id_mock',
        created_at: '10/10/2017 15:00:00',
        state: 'RJ',
        type: 'chat',
        reason: 'elogio',
        message: 'mock text'
    }
    window.fetch = jest.fn().mockImplementation(() => Promise.resolve(mock(201, mock_result)));

    const component = ReactTestUtils.renderIntoDocument(
        <Form />
    );
    let selects = ReactTestUtils.scryRenderedDOMComponentsWithTag(component, 'select');
    let textArea = ReactTestUtils.findRenderedDOMComponentWithTag(component, 'textarea');

    selects.forEach((v, i) => {
        ReactTestUtils.Simulate.change(v, {target: {type: v.type, name: v.name, value: mock_result[v.name]}});
    });
    ReactTestUtils.Simulate.change(textArea, {target: {type: 'textarea', name: 'message', value: mock_result.message}});

    let btnSave = ReactTestUtils.findRenderedDOMComponentWithClass(component, 'form_btn_submit');
    ReactTestUtils.Simulate.submit(btnSave,
        {
            preventDefault: jest.fn()
        }
    );

    sleep(1000).then(() => {
        expect(window.fetch).toHaveBeenCalled();
        expect(component.state.data).toBe(mock_result);
    });
});

it('clear form data', () => {
   let mock_data = {
        id: 'id_mock',
        created_at: '10/10/2017 15:00:00',
        state: 'RJ',
        type: 'chat',
        reason: 'elogio',
        message: 'mock text'
   };
   let mock_result = {
        id: '',
        created_at: '',
        state: '',
        type: '',
        reason: '',
        message: ''
    }

    const component = ReactTestUtils.renderIntoDocument(
        <Form />
    );
    let selects = ReactTestUtils.scryRenderedDOMComponentsWithTag(component, 'select');
    let textArea = ReactTestUtils.findRenderedDOMComponentWithTag(component, 'textarea');

    selects.forEach((v, i) => {
        ReactTestUtils.Simulate.change(v, {target: {type: v.type, name: v.name, value: mock_data[v.name]}});
    });
    ReactTestUtils.Simulate.change(textArea, {target: {type: 'textarea', name: 'message', value: mock_data.message}});

    let btnSave = ReactTestUtils.findRenderedDOMComponentWithClass(component, 'form_btn_reset');
    ReactTestUtils.Simulate.click(btnSave);

    sleep(1000).then(() => {
        expect(window.fetch).toHaveBeenCalled();
        expect(component.state.data).toBe(mock_result);
    });
});

