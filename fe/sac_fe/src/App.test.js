import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import ReactTestUtils from 'react-dom/test-utils';
import App from './App';

const mock = (status, response) => {
    return new window.Response(JSON.stringify(response), {
        status: status,
        headers: {
            'Content-type': 'application/json'
        }
    });
};


it('renders without crashing', () => {
    const component = renderer.create(
        <App />
    );
    let model = component.toJSON();
    expect(model).toMatchSnapshot();
});

it('renders report lazy component', () => {
    window.fetch = jest.fn().mockImplementation(() => Promise.resolve(mock(200, {result: []})));

    const component = ReactTestUtils.renderIntoDocument(
        <App />
    );

    let links = ReactTestUtils.scryRenderedDOMComponentsWithTag(component, 'a');
    let reportLink = undefined;
    links.forEach((v, i) => {
        if (v.href.indexOf('relatorio') !== -1) {
            ReactTestUtils.Simulate.submit(v,
                {
                    preventDefault: jest.fn()
                }
            );
        }
    });
    /*
    expect(window.fetch).not.toHaveBeenCalled();
    let model = component.toJSON();
    expect(model).toMatchSnapshot();
    */
});

