import React, { Component } from 'react';
import logo from './logo.svg';
import './Form.css';

class Form extends Component {

    constructor() {
        super();
        let bkp_data = {
            id: '',
            created_at: '',
            state: '',
            type: '',
            reason: '',
            message: ''
        };
        this.state = {
            bkp_data: bkp_data,
            data: bkp_data
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.insertRecord = this.insertRecord.bind(this);
        this.clean = this.clean.bind(this);
    }

    componentDidMount(event) {
    }

    handleInputChange(event) {
        const target = event.target;
        // const value = target.type === 'checkbox' ? target.checked : target.value;
        const value = target.value;
        const name = target.name;

        let new_data = Object.assign({}, this.state.data,
            {
                [name]: value
            }
        );
        this.setState(
            {
                data: new_data
            }
        );
    }

    insertRecord(event) {
        event.preventDefault();
        if (!this.state.data || !this.state.data.state || !this.state.data.reason) {
            return
        }
        let body_data = {
            state: this.state.data.state,
            type: this.state.data.type,
            reason: this.state.data.reason,
            message: this.state.data.message,
        }
        let url = '/api/record';
        let post_data = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body_data)
        }
        fetch(url, post_data)
          .then((resp) => resp.json())
          .then((respJson) => this.updateData(respJson));
    }

    updateData(new_data) {
        this.setState({
                data: new_data
        });
    }

    clean() {
        this.setState({
            data: this.state.bkp_data
        });
    }

    render() {
        return (
            <div>
                <div className="form_container">
                    <form name="record" method="post" onSubmit={this.insertRecord}>
                        <fieldset className="fieldset_form">
                            <legend>
                                <div>
                                    <img src={logo} className="form_header_logo" alt="logo" />
                                </div>
                            </legend>
                            <div className="form_output">
                                <div>
                                    <label>ID</label>
                                    <input type="text" disabled onChange={this.handleInputChange} value={this.state.data.id} />
                                </div>
                                <div>
                                    <label>Data</label>
                                    <input type="text" disabled onChange={this.handleInputChange} value={this.state.data.created_at} />
                                </div>
                            </div>
                            <div className="form_input combo_input">
                                <label htmlFor="state">
                                    Estado <span className="form_required_input">*</span>
                                </label>
                                <select name="state" required value={this.state.data.state} onChange={this.handleInputChange}>
                                    <option value="">-- Selecione --</option>
                                    <option value="RJ">RJ</option>
                                    <option value="SP">SP</option>
                                    <option value="MG">Mg</option>
                                    <option value="SC">SC</option>
                                </select>
                            </div>
                            <div className="form_input combo_input">
                                <label htmlFor="type">
                                    Tipo <span className="form_required_input">*</span>
                                </label>
                                <select name="type" required value={this.state.data.type} onChange={this.handleInputChange}>
                                    <option value="">-- Selecione --</option>
                                    <option value="telefone">telefone</option>
                                    <option value="chat">chat</option>
                                    <option value="email">email</option>
                                </select>
                            </div>
                            <div className="form_input combo_input">
                                <label htmlFor="state">
                                    Motivo <span className="form_required_input">*</span>
                                </label>
                                <select name="reason" required value={this.state.data.reason} onChange={this.handleInputChange}>
                                    <option value="">-- Selecione --</option>
                                    <option value="dúvida">dúvida</option>
                                    <option value="elogio">elogio</option>
                                    <option value="sugestão">sugestão</option>
                                </select>
                            </div>
                            <div className="form_input textare_input">
                                <div>
                                    <label htmlFor="state">
                                        Mensagem <span className="form_required_input">*</span>
                                    </label>
                                </div>
                                <textarea name="message" rows="10" required value={this.state.data.message} onChange={this.handleInputChange}>
                                </textarea>
                            </div>
                            <div className="action_bar">
                                <button type="submit" className="form_btn_submit">Gravar</button>
                                <button type="button" className="form_btn_reset" onClick={this.clean}>Clean</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        );
    }
}

export default Form;

