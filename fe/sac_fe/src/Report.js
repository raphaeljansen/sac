import React from "react";
import './Report.css';

const RecordRows = ({data}) => (
    data.map(
        (r, i) => (
            <tr key={i}>
                <td>{r.type}</td>
                <td>{r.reason}</td>
                <td>{r.message}</td>
            </tr>
        )
    )
)

const RecordDataTable = ({data}) => (
    <table className="record_data_table">
        <thead>
            <tr>
                <th>Tipo</th>
                <th>Motivo</th>
                <th>Mensagem</th>
            </tr>
        </thead>
        <tbody>
            <RecordRows data={data} />
        </tbody>
        <tfoot>
            <tr>
                <td colSpan="3">
                    {data.length}
                </td>
            </tr>
        </tfoot>
    </table>
)

const StateDataTable = ({data}) => (
    data.map(
        (s, i) => (
            <table key={i} className="state_data_table">
                <thead>
                    <tr>
                        <th>
                            {s.state}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <RecordDataTable data={s.data} />
                        </td>
                    </tr>
                </tbody>
            </table>
        )
    )
)

const TableData = ({data}) => (
    data.result.map(
        (d, i) => (
            <table key={i} className="report_data_table">
                <thead>
                    <tr>
                        <th>
                            {d.day_created_at}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <StateDataTable data={d.data} />
                        </td>
                    </tr>
                </tbody>
            </table>
        )
    )
)


class Report extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: {
                result: []
            }
        }
    }

    componentDidMount() {
        this.fetchReportData();
    }

    fetchReportData() {
        let url = '/api/record';
        fetch(url)
            .then((resp) => resp.json())
            .then((respJson) => this.updateData(respJson));
    }

    updateData(new_data) {
        this.setState({
            data: new_data
        })
    }

    render(){
        return(
            <div>
                <TableData data={this.state.data} />
            </div>
        );
    }
}

export default Report;

