PROJECTS := $(shell ls -dtr [fb]e/*)
export FE_PORT ?= 8090
export BE_PORT ?= 8080
export DB_PASSWORD ?=
export DB_NAME ?= postgres
export DB_PORT ?= 5432

.PHONY: default
default: coverage

.PHONY: dev
dev:
	@echo '-- [sac] dev projects="$(PROJECTS)" --'
	@$(foreach project,$(PROJECTS),$(MAKE) -C $(project) dev;)

.PHONY: test
test: clean
	@echo '-- [sac] test projects="$(PROJECTS)" --'
	@$(foreach project,$(PROJECTS),$(MAKE) -C $(project) test;)

.PHONY: coverage
coverage: clean
	@echo '-- [sac] coverage projects="$(PROJECTS)" --'
	@$(foreach project,$(PROJECTS),$(MAKE) -C $(project) coverage;)

.PHONY: itest
itest: test
	@echo '-- [sac] itest projects="$(PROJECTS)" --'
	@$(foreach project,$(PROJECTS),$(MAKE) -C $(project) itest;)

.PHONY: clean
clean:
	@echo '-- [sac] clean projects="$(PROJECTS)" --'
	@$(foreach project,$(PROJECTS),$(MAKE) -C $(project) clean;)

.PHONY: run
run:
	@echo '-- [sac] run --'
	@docker-compose build
	@docker-compose up --force-recreate
	#@docker-compose up -d --force-recreate && sleep 2 && open http://127.0.0.1:$(FE_PORT)/

.PHONY: stop
stop:
	@echo '-- [sac] stop --'
	@docker-compose down

