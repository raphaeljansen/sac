from unittest import TestCase
from urls import url_patterns
from handler.record import Record
from handler.healthcheck import HealthCheck


class UrlsTest(TestCase):

    def test_url_pattern(self):
        self.assertEqual(2, len(url_patterns))
        self.assertIn((r"/record/?", Record), url_patterns)
        self.assertIn((r"/healthcheck/?", HealthCheck), url_patterns)
