from uuid import uuid4
from datetime import datetime
from model.data import record, metadata
from unittest import TestCase
from unittest.mock import MagicMock
from model.data import list_grouped_createdat_and_state, insert
from sqlalchemy import Integer, DateTime
from sqlalchemy.dialects.postgresql import JSONB
from resource.sqlalchemy import sqlengine


class DataTest(TestCase):

    def test_db_metadata(self):
        self.assertIn('record', metadata.tables)
        self.assertIn('sq_record_id', metadata._sequences)

    def test_record_columns(self):
        self.assertEqual(1, len(record.primary_key))
        self.assertEqual(3, len(record.columns))
        self.assertIsInstance(record.columns.id.type, Integer)
        self.assertIsInstance(record.columns.created_at.type, DateTime)
        self.assertIsInstance(record.columns.data.type, JSONB)
        self.assertEqual(1, len(record.indexes))
        for idx in record.indexes:
            self.assertEqual('idx_created_at', idx.name)

    def test_insert_success(self):
        class Result():
            def __init__(self, id, created_at):
                self.id = id
                self.created_at = created_at

        mock_insert_result = Result(str(uuid4()), datetime.now())
        mock_resultset = MagicMock(name='fetchone')
        mock_resultset.fetchone.return_value = mock_insert_result

        mock_sql_conn = MagicMock(name='execute')
        mock_sql_conn.execute.return_value = mock_resultset

        sqlengine.connect = MagicMock(return_value=mock_sql_conn)

        new_record = {
            'data': {
                'type': 'reclamação',
                'state': 'RJ',
                'message': 'unit test xin xon xun',
            }
        }

        result = insert(new_record)

        self.assertTrue(mock_sql_conn.execute.called)
        self.assertTrue(mock_resultset.fetchone.called)
        self.assertIn('id', result)
        self.assertIsNotNone(result['id'])
        self.assertIn('created_at', result)
        self.assertIsNotNone(result['created_at'])

    def test_list_grouped_createdat_sales_success(self):
        class Result():
            def __init__(self, day_created_at, data):
                self.day_created_at = day_created_at
                self.data = data

        mock_select_result = []
        for k in range(5):
            mock_select_result.append(
                Result(datetime.now(), {'state': 'MK', 'data': []})
            )
        mock_resultset = MagicMock(name='fetchall')
        mock_resultset.fetchall.return_value = mock_select_result

        mock_sql_conn = MagicMock(name='execute')
        mock_sql_conn.execute.return_value = mock_resultset

        sqlengine.connect = MagicMock(return_value=mock_sql_conn)
        report = list_grouped_createdat_and_state()

        self.assertTrue(mock_sql_conn.execute.called)
        self.assertTrue(mock_resultset.fetchall.called)
        self.assertIn('result', report)
        self.assertEqual(5, len(report['result']))
