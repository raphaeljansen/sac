from datetime import datetime
from sqlalchemy import MetaData, Table, Column, Integer, DateTime, Sequence,\
                       Index, text
from sqlalchemy.dialects.postgresql import JSONB
from resource.sqlalchemy import sqlengine

metadata = MetaData()

record = Table(
    'record', metadata,
    Column('id', Integer, Sequence('sq_record_id'), primary_key=True),
    Column('created_at', DateTime, default=datetime.now),
    Column('data', JSONB),
)
Index(
    'idx_created_at', record.c.created_at,
    text("(data->'state')"), postgresql_using="gin"
)


def insert(new_record):
    insert_result = sqlengine.connect().execute(
            record.insert().returning(record.c.id, record.c.created_at),
            data=new_record,
            ).fetchone()
    return {
        'id': insert_result.id,
        'created_at': insert_result.created_at.isoformat(),
        **new_record,
    }


def list_grouped_createdat_and_state():
    sql = text('''
        select t.day_created_at, json_agg(to_json(t.*)) as data
        from (
            select date_trunc('day', created_at) as day_created_at,
                   data->'state' as state, json_agg(data) as data
            from record
            group by day_created_at, state
            order by day_created_at desc, state
        ) as t
        group by t.day_created_at
        order by t.day_created_at desc
        ''')
    result = sqlengine.connect().execute(sql).fetchall()
    data = {'result': []}
    for row in result:
        data['result'].append(
            {
                'day_created_at': row.day_created_at.strftime("%d/%m/%Y"),
                'data': row.data,
            }
        )
    return data
