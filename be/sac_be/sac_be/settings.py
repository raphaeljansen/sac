import os

settings = {
    'debug': bool(os.getenv('DEBUG', 'True')),
    'autoreload': bool(os.getenv('AUTO_RELOAD', 'True')),
    'port': os.getenv('PORT', '8080'),
    'host': os.getenv('HOST', '0.0.0.0'),
    # 'cookie_secret': 'your-cookie-secret',
    'xsrf_cookies': False,
}

dbsettings = {
    'user': os.getenv('DB_USER', 'postgres'),
    'password': os.getenv('DB_PASSWORD', 'postgres'),
    'address': '{}:{}'.format(
        os.getenv('DB_HOST', '127.0.0.1'),
        os.getenv('DB_PORT', '5432'),
    ),
    'name': os.getenv('DB_NAME', 'postgres'),
    'max_conns': int(os.getenv('DB_MAX_CONNECTIONS', '10')),
}

LOG_LEVEL = os.getenv('LOG_LEVEL', 'DEBUG')
