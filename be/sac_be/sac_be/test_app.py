from unittest import TestCase
from unittest.mock import patch, MagicMock
import tornado.ioloop
from app import main


class AppTest(TestCase):

    @patch('tornado.httpserver.HTTPServer.listen')
    def test_main_success(self, mock_listen):
        ioloop = tornado.ioloop.IOLoop.instance()
        ioloop.start = MagicMock(name='start')
        main()
        self.assertTrue(mock_listen.called)
        self.assertTrue(ioloop.start.called)

    @patch('tornado.httpserver.HTTPServer.listen')
    @patch('sys.stdout.write')
    def test_main_keyboard_interrupt(self, mock_listen, mock_sys_write):
        ioloop = tornado.ioloop.IOLoop.instance()
        ioloop.start = MagicMock(name='start', side_effect=KeyboardInterrupt)
        main()
        self.assertTrue(mock_listen.called)
        self.assertTrue(ioloop.start.called)
        self.assertTrue(
            mock_sys_write.called_with('closedby_user_interruption')
        )
