from handler.healthcheck import HealthCheck
from handler.record import Record

url_patterns = [
    (r"/record/?", Record),
    (r"/healthcheck/?", HealthCheck),
]
