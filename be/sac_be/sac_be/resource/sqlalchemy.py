from settings import dbsettings
from sqlalchemy import create_engine

sqlengine = create_engine(
    'postgresql://{}:{}@{}/{}'.format(
        dbsettings['user'],
        dbsettings['password'],
        dbsettings['address'],
        dbsettings['name'],
    ), pool_size=dbsettings['max_conns'], max_overflow=0, echo=True
)
