import logging
import sys
import settings
from __init__ import __version__


class InfoFilter(logging.Filter):
        def filter(self, rec):
            return rec.levelno in (logging.DEBUG, logging.INFO)


def logging_setup():
    log_level = settings.LOG_LEVEL
    logger = logging.getLogger()
    logger.setLevel(log_level)

    fmt = logging.Formatter(
        fmt='%(asctime)s | v{} | %(levelname)s[%(name)s] - %(message)s'
            .format(__version__),
        datefmt='%Y-%m-%d %H:%M:%S'
    )

    stdout = logging.StreamHandler(sys.stdout)
    stdout.setLevel(log_level)
    stdout.addFilter(InfoFilter())
    stdout.setFormatter(fmt)

    stderr = logging.StreamHandler(sys.stderr)
    stderr.setLevel(logging.WARNING)
    stderr.setFormatter(fmt)

    logger.addHandler(stdout)
    logger.addHandler(stderr)
