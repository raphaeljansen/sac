import logging
import tornado
import json
from model.data import insert, list_grouped_createdat_and_state

logger = logging.getLogger(__name__)


class Record(tornado.web.RequestHandler):

    async def get(self):
        logger.info('get_begin')

        report = list_grouped_createdat_and_state()
        response_body = json.dumps(report)

        self.set_header('Content-Type', 'application/json; charset=utf-8')
        self.write(response_body)
        self.set_status(200)
        logger.info('get_end')

    async def post(self):
        logger.info('post_begin')
        body = self.request.body
        if not body:
            self.set_status(400)
            return

        request_body = json.loads(self.request.body)
        response_body = insert(request_body)

        self.set_header('Content-Type', 'application/json; charset=utf-8')
        self.write(response_body)
        self.set_status(201)
        logger.info('post_end')
