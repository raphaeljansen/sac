from tornado.web import Application
from tornado.testing import AsyncHTTPTestCase
from handler.healthcheck import HealthCheck

application = Application([(r'/healthcheck/?', HealthCheck)])


class HealthCheckTest(AsyncHTTPTestCase):

    def setUp(self):
        super().setUp()

    def get_app(self):
        return application

    def test_get_success(self):
        response = self.fetch('/healthcheck/')
        self.assertEqual(response.code, 200)
        self.assertEqual(response.body, b'WORKING')
