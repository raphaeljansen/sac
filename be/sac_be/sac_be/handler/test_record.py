import json
from uuid import uuid4
from datetime import datetime
from tornado.web import Application
from tornado.testing import AsyncHTTPTestCase
from handler.record import Record
from unittest.mock import MagicMock
from resource.sqlalchemy import sqlengine

application = Application([(r'/record/?', Record)])


class RecordTest(AsyncHTTPTestCase):

    def setUp(self):
        super().setUp()

    def get_app(self):
        return application

    def test_get_success(self):
        class Result():
            def __init__(self, day_created_at, data):
                self.day_created_at = day_created_at
                self.data = data

        mock_select_result = []
        for k in range(5):
            mock_select_result.append(
                Result(datetime.now(), {'state': 'MK', 'data': []})
            )
        mock_resultset = MagicMock(name='fetchall')
        mock_resultset.fetchall.return_value = mock_select_result

        mock_sql_conn = MagicMock(name='execute')
        mock_sql_conn.execute.return_value = mock_resultset

        sqlengine.connect = MagicMock(return_value=mock_sql_conn)

        response = self.fetch('/record/')
        self.assertEqual(response.code, 200)
        self.assertIn('json', response.headers['content-type'])
        response_json = json.loads(response.body)
        self.assertIn('result', response_json)
        self.assertIsInstance(response_json['result'], list)

    def test_post_success(self):
        class Result():
            def __init__(self, id, created_at):
                self.id = id
                self.created_at = created_at

        mock_insert_result = Result(str(uuid4()), datetime.now())
        mock_resultset = MagicMock(name='fetchone')
        mock_resultset.fetchone.return_value = mock_insert_result

        mock_sql_conn = MagicMock(name='execute')
        mock_sql_conn.execute.return_value = mock_resultset

        sqlengine.connect = MagicMock(return_value=mock_sql_conn)

        request_body = json.dumps(
            {
                'data': {
                    'type': 'telefonema',
                    'state': 'SC',
                    'message': 'unit test bla bla bla',
                }
            }
        )
        response = self.fetch(
            '/record/',
            method='POST',
            body=request_body,
            headers={'Content-Type': 'application/json'},
            raise_error=False
        )
        self.assertEqual(response.code, 201)
        result = response.body
        self.assertIsNotNone(result)
        response_body = json.loads(result)
        self.assertIn('id', response_body)

    def test_post_blank_body(self):
        response = self.fetch(
            '/record/',
            method='POST',
            body=b'',
            headers={'Content-Type': 'application/json'},
            raise_error=False
        )
        self.assertEqual(response.code, 400)
        self.assertIs(response.body, b'')
