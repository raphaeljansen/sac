import json
from tornado.web import Application
from tornado.testing import AsyncHTTPTestCase
from handler.record import Record

application = Application([(r'/record/?', Record)])


class RecordIntegrationTest(AsyncHTTPTestCase):

    def setUp(self):
        super().setUp()

    def get_app(self):
        return application

    def test_get_success(self):
        response = self.fetch('/record/')
        self.assertEqual(response.code, 200)
        self.assertIn('json', response.headers['content-type'])
        response_json = json.loads(response.body)
        self.assertIn('result', response_json)
        self.assertIsInstance(response_json['result'], list)

    def test_post_success(self):
        request_body = json.dumps(
            {
                'data': {
                    'type': 'telefonema',
                    'state': 'SC',
                    'message': 'unit test bla bla bla',
                }
            }
        )
        response = self.fetch(
            '/record/',
            method='POST',
            body=request_body,
            headers={'Content-Type': 'application/json'},
            raise_error=False
        )
        self.assertEqual(response.code, 201)
        result = response.body
        self.assertIsNotNone(result)
        response_body = json.loads(result)
        self.assertIn('id', response_body)

    def test_post_blank_body(self):
        response = self.fetch(
            '/record/',
            method='POST',
            body=b'',
            headers={'Content-Type': 'application/json'},
            raise_error=False
        )
        self.assertEqual(response.code, 400)
        self.assertIs(response.body, b'')
