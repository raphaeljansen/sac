import tornado


class HealthCheck(tornado.web.RequestHandler):
    def get(self):
        self.set_status(200)
        self.write('WORKING')
