#!/usr/bin/env python
import sys
import tornado.httpserver
import tornado.ioloop
import tornado.web
import logging
from settings import settings, dbsettings
from config.logging import logging_setup
from urls import url_patterns


logger = logging.getLogger('sac_be')


class TornadoApp(tornado.web.Application):
    def __init__(self):
        tornado.web.Application.__init__(
            self, url_patterns, **settings)


def main():
    logging_setup()
    app = TornadoApp()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(settings['port'], settings['host'])

    try:
        logger.info(
            'start host={} port={} db_addr={} db_user={} db_name={}'
            .format(
                settings['host'],
                settings['port'],
                dbsettings['address'],
                dbsettings['user'],
                dbsettings['name'],
            )
        )
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()
        sys.stdout.write('\n')
        sys.stdout.write('closedby_user_interruption')
        sys.stdout.write('\n')


if __name__ == "__main__":
    main()
