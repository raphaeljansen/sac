-- fake record --
insert into record values
    -- 3 days ago -
    (nextval('sq_record_id'), now() - interval '3 day', '{"state": "SP", "type": "telefone", "message": "bla bla bla"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '3 day 1 minute', '{"state": "SP", "type": "telefone", "message": "bla bla bla"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '3 day 2 minute', '{"state": "SP", "type": "telefone", "message": "bla bla bla"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '3 day 3 minute', '{"state": "RJ", "type": "telefone", "message": "bla bla bla"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '3 day 4 minute', '{"state": "RJ", "type": "telefone", "message": "bla bla bla"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '3 day 5 minute', '{"state": "SC", "type": "telefone", "message": "bla bla bla"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '3 day 6 minute', '{"state": "SC", "type": "telefone", "message": "bla bla bla"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '3 day 7 minute', '{"state": "SC", "type": "telefone", "message": "bla bla bla"}'::jsonb),
    -- 2 days ago -
    (nextval('sq_record_id'), now() - interval '2 day', '{"state": "SP", "type": "telefone", "message": "hoje bla bla bla sp"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '2 day 1 minute', '{"state": "SP", "type": "telefone", "message": "hoje bla bla bla sp"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '2 day 2 minute', '{"state": "RJ", "type": "email", "message": "hoje email chato pra ler rj"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '2 day 3 minute', '{"state": "RJ", "type": "email", "message": "hoje email chato pra ler rj"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '2 day 4 minute', '{"state": "SC", "type": "chat", "message": "bla bla bla chat pra ti sc"}'::jsonb),
    (nextval('sq_record_id'), now() - interval '2 day 5 minute', '{"state": "SC", "type": "chat", "message": "bla bla bla chat pra ti sc"}'::jsonb),
    -- 1 days ago -
    (nextval('sq_record_id'), now() - interval '1 day', '{"state": "SP", "type": "email", "message": "email ble ble sp..."}'::jsonb),
    (nextval('sq_record_id'), now() - interval '1 day 1 minute', '{"state": "RJ", "type": "chat", "message": "chat xa xe xi rj"}'::jsonb);

