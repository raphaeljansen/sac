create table record (
   id integer primary key,
   created_at timestamp default now(),
   data jsonb not null
);
create sequence sq_record_id increment by 1 start with 1;
create index idx_record_createdat_state on record(created_at desc, (data->'state'));
